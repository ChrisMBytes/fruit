package chris.test.fruit.datasources.network.fruit.netsources

import chris.test.fruit.datasources.network.fruit.netsources.models.FruitListNet
import chris.test.fruit.datasources.network.fruit.FruitRestServiceApi
import io.reactivex.Single

class FruitNetSource(private val fruitRestServiceApi: FruitRestServiceApi): FruitNetSourceApi {

    override fun getData(): Single<FruitListNet> {
        return fruitRestServiceApi.data
    }
}
