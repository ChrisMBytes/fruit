package chris.test.fruit.view.recyclerview.fruit.navigator;

import android.content.Context;
import android.content.Intent;

import chris.test.fruit.view.detail.DetailActivity;
import chris.test.fruit.presentation.events.stores.Publish;
import chris.test.fruit.domain.services.fruit.models.Fruit;


public final class ItemNavigator {

    private ItemNavigator() {
        // do not instantiate
    }

    public static void navigateToDetail(final Context context, final Publish<Fruit> fruitPublish, final Fruit fruit) {
        fruitPublish.publish(fruit);
        final Intent intent = new Intent(context, DetailActivity.class);
        context.startActivity(intent);
    }
}
