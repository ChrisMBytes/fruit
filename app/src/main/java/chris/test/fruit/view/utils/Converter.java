package chris.test.fruit.view.utils;

public final class Converter {
    private Converter() {
        // do not initialise
    }

    public static String convertPenceToPounds(final String pence) {
        final int penceValue = Integer.valueOf(pence);
        return "£" + penceValue / 100 + "." + penceValue % 100;
    }

    public static String convertGramsToKilograms(final String grams) {
        final int gramsValue = Integer.valueOf(grams);
        return gramsValue / 1000 + "." + gramsValue % 1000 + " kg";
    }

    public static String convertFirstLetterToUpperCase(final String type) {
        return type.substring(0, 1).toUpperCase() + type.substring(1);
    }
}
