package chris.test.fruit.datasources.network.statistics.netsources;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chris.test.fruit.datasources.network.statistics.StatisticsRestServiceApi;
import chris.test.fruit.rules.RxImmediateSchedulerRule;
import io.reactivex.Completable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsNetSourceTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private StatisticsRestServiceApi mockStatisticsRestServiceApi;
    @InjectMocks
    private StatisticsNetSource sut;

    @Test
    public void whenSendStatsThenCallServiceApiSendStats() {
        final String event = "event";
        final String data = "data";
        when(mockStatisticsRestServiceApi.sendStats(event, data)).thenReturn(Completable.complete());

        sut.sendStats(event, data)
                .test()
                .assertComplete()
                .assertNoErrors();
        verify(mockStatisticsRestServiceApi).sendStats(event, data);
    }
}
