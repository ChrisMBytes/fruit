package chris.test.fruit.presentation.detail.viewmodels

import android.databinding.ObservableField

import chris.test.fruit.presentation.events.stores.FruitStore
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore
import chris.test.fruit.domain.services.fruit.models.Fruit
import chris.test.fruit.presentation.statistics.StatisticEvents
import chris.test.fruit.view.utils.Converter
import io.reactivex.Completable

class DetailActivityViewModel(
        private val fruitStore: FruitStore,
        private val statisticsMetricsStore: StatisticsMetricsStore,
        private val statisticEvents: StatisticEvents
): DetailActivityViewModelApi {
    override val type = ObservableField<String>()
    override val price = ObservableField<String>()
    override val weight = ObservableField<String>()
    override val imageUrl = ObservableField<String>()

    override fun init(): Completable {
        return fruitStore.observe()
                .doOnNext{ setItem(it) }
                .flatMap { statisticsMetricsStore.observe() }
                .flatMap { start ->
                    statisticEvents.displayEvent((System.currentTimeMillis() - start).toString())
                            .toObservable<Any>()
                }
                .ignoreElements()
    }

    private fun setItem(fruit: Fruit) {
        type.set(Converter.convertFirstLetterToUpperCase(fruit.type))
        price.set(Converter.convertPenceToPounds(fruit.price))
        weight.set(Converter.convertGramsToKilograms(fruit.weight))
        imageUrl.set(fruit.image)
    }
}
