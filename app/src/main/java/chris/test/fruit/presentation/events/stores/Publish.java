package chris.test.fruit.presentation.events.stores;

public interface Publish<T> {
    void publish(T value);
}
