package chris.test.fruit.data.fruit.repositories.mappers

import chris.test.fruit.datasources.network.fruit.netsources.models.FruitNet
import chris.test.fruit.domain.services.fruit.models.Fruit

object FruitMapper {

    fun toFruits(fruitNetList: List<FruitNet>): List<Fruit> {
        return fruitNetList.map { toFruit(it) }
    }

    fun toFruit(fruitNet: FruitNet): Fruit {
        return Fruit(
                fruitNet.type ?: "",
                fruitNet.price ?: "",
                fruitNet.weight ?: ""
        )
    }
}
