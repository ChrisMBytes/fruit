package chris.test.fruit.presentation.home.viewmodels

import android.databinding.ObservableBoolean
import chris.test.fruit.domain.services.fruit.models.Fruit
import chris.test.fruit.presentation.events.stores.FruitStore
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore
import chris.test.fruit.view.recyclerview.listeners.RefreshAction
import io.reactivex.Completable

interface MainActivityViewModelApi: RefreshAction {
    val isGrid: ObservableBoolean
    fun init(): Completable
    fun getFruitStore(): FruitStore
    fun getStatisticsMetricsStore(): StatisticsMetricsStore
    fun getFruitList(): List<Fruit>
    fun toggleIsGrid()
}