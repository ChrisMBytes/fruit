package chris.test.fruit.presentation.recyclerview.fruit.viewmodels;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chris.test.fruit.presentation.events.stores.FruitStore;
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore;
import chris.test.fruit.domain.services.fruit.models.Fruit;
import chris.test.fruit.view.utils.Converter;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FruitItemViewModelTest {
    @Mock
    private FruitStore mockFruitStore;
    @Mock
    private StatisticsMetricsStore mockStatisticsMetricsStore;
    @Mock
    private Fruit mockFruit;
    @InjectMocks
    private FruitItemViewModel sut;

    @Before
    public void setup() {
        when(mockFruit.getType()).thenReturn("apple");
        when(mockFruit.getImage()).thenReturn("url");
    }

    @Test
    public void whenPublishThenCallStoresPublish() {
        doNothing().when(mockFruitStore).publish(mockFruit);
        doNothing().when(mockStatisticsMetricsStore).publish(anyLong());

        sut.publish(mockFruit);

        verify(mockFruitStore).publish(mockFruit);
        verify(mockStatisticsMetricsStore).publish(anyLong());
    }

    @Test
    public void whenSetItemThenValuesAreSet() {
        sut.setItem(mockFruit);

        assertEquals(sut.getType().get(), Converter.convertFirstLetterToUpperCase(mockFruit.getType()));
        assertEquals(sut.getImageUrl().get(), mockFruit.getImage());
    }
}
