package chris.test.fruit.domain.services.statistics.models;

public enum Events {
    load("load"),
    display("display"),
    error("error");

    private final String name;

    Events(String s) {
        name = s;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
