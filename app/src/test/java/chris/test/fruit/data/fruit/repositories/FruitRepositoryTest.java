package chris.test.fruit.data.fruit.repositories;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import chris.test.fruit.datasources.network.fruit.netsources.FruitNetSourceApi;
import chris.test.fruit.rules.RxImmediateSchedulerRule;
import chris.test.fruit.datasources.network.fruit.netsources.FruitNetSource;
import chris.test.fruit.datasources.network.fruit.netsources.models.FruitListNet;
import chris.test.fruit.datasources.network.fruit.netsources.models.FruitNet;
import io.reactivex.Single;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FruitRepositoryTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private FruitNetSourceApi mockFruitNetSource;
    @InjectMocks
    private FruitRepository sut;

    @Test
    public void whenGetDataThenCallNetSourceGetData() {
        final FruitListNet fruitListNet = new FruitListNet();
        final FruitNet fruitNet = new FruitNet();
        fruitNet.setType("apple");
        final List<FruitNet> fruitNetList = new ArrayList<>();
        fruitNetList.add(fruitNet);
        fruitListNet.setFruit(fruitNetList);
        when(mockFruitNetSource.getData()).thenReturn(Single.just(fruitListNet));

        sut.getData()
                .test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(fruits -> fruits.get(0).getType().equals(fruitListNet.getFruit().get(0).getType()));
        verify(mockFruitNetSource).getData();
    }
}
