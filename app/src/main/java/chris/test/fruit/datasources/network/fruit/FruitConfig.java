package chris.test.fruit.datasources.network.fruit;

import chris.test.fruit.datasources.network.RetrofitConfig;

public class FruitConfig implements RetrofitConfig {
    @Override
    public String getBaseUrl() {
        return "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/";
    }
}
