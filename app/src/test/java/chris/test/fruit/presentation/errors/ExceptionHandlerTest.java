package chris.test.fruit.presentation.errors;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintWriter;
import java.io.StringWriter;

import chris.test.fruit.rules.RxImmediateSchedulerRule;
import chris.test.fruit.presentation.statistics.StatisticEvents;
import io.reactivex.Completable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExceptionHandlerTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private StatisticEvents mockStatisticEvents;
    @InjectMocks
    private ExceptionHandler sut;

    @Test
    public void whenUncaughtExceptionThenCallStatisticEventsErrorEvent() {
        final Throwable exception = new Throwable();
        StackTraceElement[] trace = new StackTraceElement[] {
                new StackTraceElement("ClassName","methodName","fileName",10)
        };
        exception.setStackTrace(trace);
        final StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        final String errorReport = stackTrace.toString();

        when(mockStatisticEvents.errorEvent(errorReport)).thenReturn(Completable.complete());

        sut.uncaughtException(new Thread(), exception);

        verify(mockStatisticEvents).errorEvent(errorReport);
    }
}
