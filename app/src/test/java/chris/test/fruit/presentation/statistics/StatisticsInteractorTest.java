package chris.test.fruit.presentation.statistics;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chris.test.fruit.domain.services.statistics.models.Events;
import chris.test.fruit.domain.services.statistics.StatisticsService;
import io.reactivex.Completable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsInteractorTest {
    @Mock
    private StatisticsService mockStatisticsService;
    @InjectMocks
    private StatisticsInteractor sut;

    @Test
    public void whenLoadEventThenCallSendStatsWithExpectedValues() {
        final String data = "load";
        when(mockStatisticsService.sendStats(Events.load, data)).thenReturn(Completable.complete());

        sut.loadEvent(data)
                .test()
                .assertNoErrors()
                .assertComplete();
        verify(mockStatisticsService).sendStats(Events.load, data);
    }

    @Test
    public void whenDisplayEventThenCallSendStatsWithExpectedValues() {
        final String data = "display";
        when(mockStatisticsService.sendStats(Events.display, data)).thenReturn(Completable.complete());

        sut.displayEvent(data)
                .test()
                .assertNoErrors()
                .assertComplete();
        verify(mockStatisticsService).sendStats(Events.display, data);
    }

    @Test
    public void whenErrorEventThenCallSendStatsWithExpectedValues() {
        final String data = "error";
        when(mockStatisticsService.sendStats(Events.error, data)).thenReturn(Completable.complete());

        sut.errorEvent(data)
                .test()
                .assertNoErrors()
                .assertComplete();
        verify(mockStatisticsService).sendStats(Events.error, data);
    }
}
