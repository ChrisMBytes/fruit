package chris.test.fruit.datasources.network.fruit.netsources

import chris.test.fruit.datasources.network.fruit.netsources.models.FruitListNet
import io.reactivex.Single

interface FruitNetSourceApi {
    fun getData(): Single<FruitListNet>
}