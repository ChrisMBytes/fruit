package chris.test.fruit.di;

import chris.test.fruit.domain.services.fruit.FruitServiceApi;
import chris.test.fruit.presentation.detail.viewmodels.DetailActivityViewModel;
import chris.test.fruit.presentation.detail.viewmodels.DetailActivityViewModelApi;
import chris.test.fruit.presentation.events.stores.FruitStore;
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore;
import chris.test.fruit.presentation.home.viewmodels.MainActivityViewModel;
import chris.test.fruit.presentation.home.viewmodels.MainActivityViewModelApi;
import chris.test.fruit.presentation.statistics.StatisticEvents;
import dagger.Module;
import dagger.Provides;

@Module
public class ViewModelsModule {

    @Provides
    MainActivityViewModelApi providesMainActivityViewModel(FruitServiceApi fruitService,
                                                           FruitStore fruitStore,
                                                           StatisticEvents statisticEvents,
                                                           StatisticsMetricsStore statisticsMetricsStore) {
        return new MainActivityViewModel(fruitService, fruitStore, statisticEvents, statisticsMetricsStore);
    }

    @Provides
    DetailActivityViewModelApi providesDetailActivityViewModel(FruitStore fruitStore,
                                                               StatisticsMetricsStore statisticsMetricsStore,
                                                               StatisticEvents statisticEvents) {
        return new DetailActivityViewModel(fruitStore, statisticsMetricsStore, statisticEvents);
    }
}
