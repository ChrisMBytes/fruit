package chris.test.fruit.datasources.network.fruit.netsources;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import chris.test.fruit.rules.RxImmediateSchedulerRule;
import chris.test.fruit.datasources.network.fruit.netsources.models.FruitListNet;
import chris.test.fruit.datasources.network.fruit.netsources.models.FruitNet;
import chris.test.fruit.datasources.network.fruit.FruitRestServiceApi;
import io.reactivex.Single;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FruitNetSourceTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private FruitRestServiceApi mockFruitRestServiceApi;
    @InjectMocks
    private FruitNetSource sut;

    @Test
    public void whenGetDataThenCallMockyServiceGetData() {
        final FruitListNet fruitListNet = new FruitListNet();
        final FruitNet fruitNet = new FruitNet();
        fruitNet.setType("apple");
        final List<FruitNet> fruitNetList = new ArrayList<>();
        fruitNetList.add(fruitNet);
        fruitListNet.setFruit(fruitNetList);
        when(mockFruitRestServiceApi.getData()).thenReturn(Single.just(fruitListNet));

        sut.getData()
                .test()
                .assertComplete()
                .assertNoErrors()
                .assertValue(fruitListNet1 -> fruitListNet1.getFruit().get(0).getType().equals(fruitNetList.get(0).getType()));
        verify(mockFruitRestServiceApi).getData();
    }
}
