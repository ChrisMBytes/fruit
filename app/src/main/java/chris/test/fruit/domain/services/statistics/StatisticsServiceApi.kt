package chris.test.fruit.domain.services.statistics

import chris.test.fruit.domain.services.statistics.models.Events
import io.reactivex.Completable

interface StatisticsServiceApi {
    fun sendStats(event: Events, data: String): Completable
}