package chris.test.fruit.presentation.recyclerview.fruit.viewmodels

import android.databinding.ObservableField
import chris.test.fruit.domain.services.fruit.models.Fruit
import chris.test.fruit.presentation.events.stores.Publish
import chris.test.fruit.presentation.recyclerview.viewmodels.ItemViewModel

interface FruitItemViewModelApi: ItemViewModel<Fruit>, Publish<Fruit> {
    val type: ObservableField<String>
    val imageUrl: ObservableField<String>
    fun getFruit(): Fruit
}