package chris.test.fruit.presentation.recyclerview.fruit.viewmodels

import android.databinding.ObservableField

import chris.test.fruit.presentation.events.stores.FruitStore
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore
import chris.test.fruit.domain.services.fruit.models.Fruit
import chris.test.fruit.presentation.recyclerview.viewmodels.ItemViewModel
import chris.test.fruit.view.utils.Converter

class FruitItemViewModel(
        private val fruitStore: FruitStore,
        private val statisticsMetricsStore: StatisticsMetricsStore
): FruitItemViewModelApi {
    private lateinit var fruit: Fruit
    override val type = ObservableField<String>()
    override val imageUrl = ObservableField<String>()

    override fun getFruit(): Fruit = fruit

    override fun setItem(fruit: Fruit) {
        this.fruit = fruit
        type.set(Converter.convertFirstLetterToUpperCase(fruit.type))
        imageUrl.set(fruit.image)
    }

    override fun publish(fruit: Fruit) {
        statisticsMetricsStore.publish(System.currentTimeMillis())
        fruitStore.publish(fruit)
    }
}
