package chris.test.fruit.di;

import chris.test.fruit.view.detail.DetailActivity;
import chris.test.fruit.view.home.MainActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = ViewModelsModule.class)
    abstract MainActivity mainActivity();

    @ContributesAndroidInjector(modules = ViewModelsModule.class)
    abstract DetailActivity detailActivity();
}