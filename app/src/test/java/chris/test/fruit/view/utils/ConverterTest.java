package chris.test.fruit.view.utils;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class ConverterTest {
    @Test
    public void whenConvertPenceToPoundsThenExpectedResult() {
        final String pence = "60";
        final String pounds = "£0.60";

        assertEquals(Converter.convertPenceToPounds(pence), pounds);
    }

    @Test
    public void whenConvertGramsToKilogramsThenExpectedResult() {
        final String grams = "600";
        final String kilograms = "0.600 kg";

        assertEquals(Converter.convertGramsToKilograms(grams), kilograms);
    }

    @Test
    public void whenConvertFirstLetterToUpperCaseThenExpectedResult() {
        final String type = "apple";
        final String expectedType = "Apple";

        assertEquals(Converter.convertFirstLetterToUpperCase(type), expectedType);
    }
}
