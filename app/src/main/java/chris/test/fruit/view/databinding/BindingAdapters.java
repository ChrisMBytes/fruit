package chris.test.fruit.view.databinding;

import android.databinding.BindingAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import chris.test.fruit.R;

public class BindingAdapters {

    @BindingAdapter("loadImage")
    public static void loadImage(final ImageView imageView, final String imageUrl) {
        Glide.with(imageView.getContext())
                .load(imageUrl)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(imageView);
    }

    @BindingAdapter("toggleLayoutManager")
    public static void toggleLayoutManager(final RecyclerView recyclerView, final boolean isGrid) {
        recyclerView.setLayoutManager(isGrid ?
                new GridLayoutManager(recyclerView.getContext(), 2) :
                new LinearLayoutManager(recyclerView.getContext()));
    }
}
