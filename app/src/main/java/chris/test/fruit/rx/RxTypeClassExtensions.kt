package chris.test.fruit.rx

import arrow.core.Either
import io.reactivex.Single

fun <L, R> Single<R>.toEither(func: (Throwable) -> L): Single<Either<L, R>> =
        map { Either.right(it) as Either<L, R> }
                .onErrorReturn { Either.left(func(it)) }