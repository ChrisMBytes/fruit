package chris.test.fruit.domain.services.fruit.models

data class Fruit(
    val type: String = "",
    val price: String = "",
    val weight: String = "",
    val image: String = ""
)
