package chris.test.fruit.presentation.statistics;

import io.reactivex.Completable;

public interface StatisticEvents {
    Completable loadEvent(String data);

    Completable displayEvent(String data);

    Completable errorEvent(String data);
}
