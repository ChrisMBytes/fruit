package chris.test.fruit.presentation.home.viewmodels

import android.databinding.ObservableBoolean
import chris.test.fruit.domain.services.fruit.FruitServiceApi
import chris.test.fruit.domain.services.fruit.models.Fruit
import chris.test.fruit.presentation.events.stores.FruitStore
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore
import chris.test.fruit.presentation.statistics.StatisticEvents
import chris.test.fruit.rx.RxTransformers.applyCompletableSchedulers
import chris.test.fruit.rx.RxTransformers.consumeCompletableError
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.Completable
import io.reactivex.Observable

class MainActivityViewModel(
        private val fruitServiceApi: FruitServiceApi,
        private val fruitStore: FruitStore,
        private val statisticEvents: StatisticEvents,
        private val statisticsMetricsStore: StatisticsMetricsStore
) : MainActivityViewModelApi {
    override val isGrid = ObservableBoolean(false)

    private val onFinished = PublishRelay.create<Boolean>()

    private var fruitList: List<Fruit> = emptyList()

    override fun init(): Completable {
        val start = System.currentTimeMillis()
        return fruitServiceApi.getData()
                .doOnSuccess { fruits ->
                    fruitList = fruits
                    onFinished.accept(false)
                }
                .toCompletable()
                .mergeWith(statisticEvents.loadEvent((System.currentTimeMillis() - start).toString()))
    }

    override fun getFruitStore(): FruitStore {
        return fruitStore
    }

    override fun getStatisticsMetricsStore(): StatisticsMetricsStore {
        return statisticsMetricsStore
    }

    override fun getFruitList(): List<Fruit> {
        return fruitList
    }

    override fun toggleIsGrid() {
        isGrid.set(!isGrid.get())
    }

    override fun refresh() {
        init().compose(applyCompletableSchedulers())
                .compose(consumeCompletableError())
                .subscribe()
    }

    override fun onFinished(): Observable<Boolean> {
        return onFinished.hide()
    }
}
