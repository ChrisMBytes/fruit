package chris.test.fruit.di;

import chris.test.fruit.domain.services.statistics.StatisticsServiceApi;
import chris.test.fruit.presentation.statistics.StatisticEvents;
import chris.test.fruit.presentation.statistics.StatisticsInteractor;
import dagger.Module;
import dagger.Provides;

@Module
public class InteractorsModule {
    @Provides
    StatisticEvents providesStatisticsInteractor(StatisticsServiceApi statisticsService) {
        return new StatisticsInteractor(statisticsService);
    }
}
