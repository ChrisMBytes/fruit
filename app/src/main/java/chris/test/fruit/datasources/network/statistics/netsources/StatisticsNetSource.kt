package chris.test.fruit.datasources.network.statistics.netsources

import chris.test.fruit.datasources.network.statistics.StatisticsRestServiceApi
import io.reactivex.Completable

class StatisticsNetSource(
        private val statisticsRestServiceApi: StatisticsRestServiceApi
) : StatisticsNetSourceApi {

    override fun sendStats(event: String, data: String): Completable {
        return statisticsRestServiceApi.sendStats(event, data)
    }
}
