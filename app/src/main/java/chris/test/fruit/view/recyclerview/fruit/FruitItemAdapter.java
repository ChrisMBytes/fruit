package chris.test.fruit.view.recyclerview.fruit;

import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import chris.test.fruit.R;
import chris.test.fruit.databinding.FruitItemBinding;
import chris.test.fruit.presentation.events.stores.FruitStore;
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore;
import chris.test.fruit.view.recyclerview.DataBindingRecyclerViewAdapter;
import chris.test.fruit.presentation.recyclerview.fruit.viewmodels.FruitItemViewModel;
import chris.test.fruit.domain.services.fruit.models.Fruit;

public class FruitItemAdapter extends DataBindingRecyclerViewAdapter<Fruit, FruitItemViewModel> {
    private final FruitStore fruitStore;
    private final StatisticsMetricsStore statisticsMetricsStore;

    public FruitItemAdapter(final List<Fruit> fruitList, FruitStore fruitStore, StatisticsMetricsStore statisticsMetricsStore) {
        super(fruitList);
        this.fruitStore = fruitStore;
        this.statisticsMetricsStore = statisticsMetricsStore;
    }

    @NonNull
    @Override
    public ItemViewHolder<Fruit, FruitItemViewModel> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fruit_item, parent, false);
        final FruitItemViewModel viewModel = new FruitItemViewModel(fruitStore, statisticsMetricsStore);
        final FruitItemBinding binding = FruitItemBinding.bind(itemView);
        binding.setViewModel(viewModel);

        return new FruitViewHolder(itemView, binding, viewModel);
    }

    static class FruitViewHolder extends ItemViewHolder<Fruit, FruitItemViewModel> {

        FruitViewHolder(View itemView, ViewDataBinding binding, FruitItemViewModel viewModel) {
            super(itemView, binding, viewModel);
        }
    }
}
