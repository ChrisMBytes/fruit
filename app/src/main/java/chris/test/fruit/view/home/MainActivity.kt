package chris.test.fruit.view.home

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import chris.test.fruit.R
import chris.test.fruit.databinding.ActivityMainBinding
import chris.test.fruit.presentation.errors.ExceptionHandler
import chris.test.fruit.presentation.home.viewmodels.MainActivityViewModel
import chris.test.fruit.presentation.home.viewmodels.MainActivityViewModelApi
import chris.test.fruit.presentation.statistics.StatisticEvents
import chris.test.fruit.rx.RxTransformers.*
import chris.test.fruit.view.BaseActivity
import chris.test.fruit.view.recyclerview.fruit.FruitItemAdapter
import chris.test.fruit.view.recyclerview.listeners.Refresh
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var statisticEvents: StatisticEvents
    @Inject
    lateinit var mainActivityViewModel: MainActivityViewModelApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(
                this,
                R.layout.activity_main
        )
        setSupportActionBar(binding.fruitToolbar)

        if (savedInstanceState == null) {
            AndroidInjection.inject(this)
            setExceptionHandling()
            disposables.add(mainActivityViewModel.init()
                    .compose(applyCompletableSchedulers())
                    .compose(consumeCompletableError())
                    .subscribe { setup(binding) })
        } else {
            mainActivityViewModel = lastCustomNonConfigurationInstance as MainActivityViewModel
            setup(binding)
        }
    }

    private fun setExceptionHandling() {
        if (Thread.getDefaultUncaughtExceptionHandler() !is ExceptionHandler) {
            Thread.setDefaultUncaughtExceptionHandler(ExceptionHandler(statisticEvents))
        }
    }

    private fun setup(binding: ActivityMainBinding) {
        binding.apply {
            fruitRecyclerView.adapter = FruitItemAdapter(
                    mainActivityViewModel.getFruitList(),
                    mainActivityViewModel.getFruitStore(),
                    mainActivityViewModel.getStatisticsMetricsStore()
            )
            viewModel = mainActivityViewModel
            swipeRefreshLayout.setOnRefreshListener(Refresh(mainActivityViewModel))
            setupRefresh(this)
        }
    }

    private fun setupRefresh(binding: ActivityMainBinding) {
        disposables.add(
                mainActivityViewModel.onFinished()
                        .compose(applySchedulers())
                        .compose(consumeError())
                        .subscribe { binding.swipeRefreshLayout.isRefreshing = it }
        )
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        return mainActivityViewModel
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.actions, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_fruit_toggle) {
            mainActivityViewModel.toggleIsGrid()
        }
        return super.onOptionsItemSelected(item)
    }
}
