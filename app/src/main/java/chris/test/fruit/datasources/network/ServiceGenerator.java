package chris.test.fruit.datasources.network;

public interface ServiceGenerator {
    <Service> Service createService(Class<Service> serviceClass);
}
