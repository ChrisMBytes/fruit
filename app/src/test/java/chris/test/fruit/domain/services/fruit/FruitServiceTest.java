package chris.test.fruit.domain.services.fruit;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import chris.test.fruit.rules.RxImmediateSchedulerRule;
import chris.test.fruit.data.fruit.repositories.FruitRepository;
import chris.test.fruit.domain.services.fruit.models.Fruit;
import io.reactivex.Single;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FruitServiceTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private Fruit mockFruit;
    @Mock
    private FruitRepositoryApi mockFruitRepository;
    @InjectMocks
    private FruitService sut;

    @Test
    public void whenGetDataThenCallRepositoryGetData() {
        when(mockFruit.getType()).thenReturn("apple");
        final List<Fruit> fruits = new ArrayList<>();
        fruits.add(mockFruit);
        when(mockFruitRepository.getData()).thenReturn(Single.just(fruits));

        sut.getData()
                .test()
                .assertNoErrors()
                .assertComplete()
                .assertValue(fruits1 -> fruits1.get(0).getType().equals(fruits.get(0).getType()));
        verify(mockFruitRepository).getData();
    }
}
