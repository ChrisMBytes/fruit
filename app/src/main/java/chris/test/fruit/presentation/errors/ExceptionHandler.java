package chris.test.fruit.presentation.errors;

import java.io.PrintWriter;
import java.io.StringWriter;

import chris.test.fruit.presentation.statistics.StatisticEvents;

import static chris.test.fruit.rx.RxTransformers.applyCompletableSchedulers;
import static chris.test.fruit.rx.RxTransformers.consumeCompletableError;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final StatisticEvents statisticEvents;

    public ExceptionHandler(StatisticEvents statisticEvents) {
        this.statisticEvents = statisticEvents;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable exception) {
        final StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        final String errorReport = stackTrace.toString();

        statisticEvents.errorEvent(errorReport)
                .compose(applyCompletableSchedulers())
                .compose(consumeCompletableError())
                .subscribe();
    }
}
