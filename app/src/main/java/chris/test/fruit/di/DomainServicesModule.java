package chris.test.fruit.di;

import javax.inject.Singleton;

import chris.test.fruit.datasources.network.fruit.netsources.FruitNetSourceApi;
import chris.test.fruit.datasources.network.statistics.netsources.StatisticsNetSourceApi;
import chris.test.fruit.domain.services.fruit.FruitRepositoryApi;
import chris.test.fruit.domain.services.fruit.FruitService;
import chris.test.fruit.domain.services.fruit.FruitServiceApi;
import chris.test.fruit.data.fruit.repositories.FruitRepository;
import chris.test.fruit.datasources.network.fruit.netsources.FruitNetSource;
import chris.test.fruit.datasources.network.RetrofitServiceGenerator;
import chris.test.fruit.datasources.network.fruit.FruitConfig;
import chris.test.fruit.datasources.network.fruit.FruitRestServiceApi;
import chris.test.fruit.datasources.network.statistics.StatisticsConfig;
import chris.test.fruit.datasources.network.statistics.StatisticsRestServiceApi;
import chris.test.fruit.domain.services.statistics.StatisticsRepositoryApi;
import chris.test.fruit.domain.services.statistics.StatisticsService;
import chris.test.fruit.data.statistics.repositories.StatisticsRepository;
import chris.test.fruit.datasources.network.statistics.netsources.StatisticsNetSource;
import chris.test.fruit.domain.services.statistics.StatisticsServiceApi;
import dagger.Module;
import dagger.Provides;

@Module
public class DomainServicesModule {

    @Provides
    FruitConfig providesFruitConfig() {
        return new FruitConfig();
    }

    @Provides
    StatisticsConfig providesStatisticsConfig() {
        return new StatisticsConfig();
    }

    @Singleton
    @Provides
    StatisticsRestServiceApi providesStatisticsRestServiceApi(StatisticsConfig statisticsConfig) {
        return new RetrofitServiceGenerator(statisticsConfig).createService(StatisticsRestServiceApi.class);
    }

    @Provides
    StatisticsNetSourceApi providesStatisticsNetSource(StatisticsRestServiceApi statisticsRestServiceApi) {
        return new StatisticsNetSource(statisticsRestServiceApi);
    }

    @Provides
    StatisticsRepositoryApi providesStatisticsRepository(StatisticsNetSourceApi statisticsNetSourceApi) {
        return new StatisticsRepository(statisticsNetSourceApi);
    }

    @Provides
    StatisticsServiceApi providesStatisticsService(StatisticsRepositoryApi statisticsRepositoryApi) {
        return new StatisticsService(statisticsRepositoryApi);
    }

    @Singleton
    @Provides
    FruitRestServiceApi providesFruitRestServiceApi(FruitConfig fruitConfig) {
        return new RetrofitServiceGenerator(fruitConfig).createService(FruitRestServiceApi.class);
    }

    @Provides
    FruitNetSourceApi providesFruitNetSource(FruitRestServiceApi fruitRestServiceApi) {
        return new FruitNetSource(fruitRestServiceApi);
    }

    @Provides
    FruitRepositoryApi providesFruitRepository(FruitNetSourceApi fruitNetSourceApi) {
        return new FruitRepository(fruitNetSourceApi);
    }

    @Provides
    FruitServiceApi providesFruitService(FruitRepositoryApi fruitRepositoryApi) {
        return new FruitService(fruitRepositoryApi);
    }
}
