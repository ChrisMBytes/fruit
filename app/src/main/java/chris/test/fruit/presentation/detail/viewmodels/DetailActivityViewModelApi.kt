package chris.test.fruit.presentation.detail.viewmodels

import android.databinding.ObservableField
import io.reactivex.Completable

interface DetailActivityViewModelApi {
    val type: ObservableField<String>
    val price: ObservableField<String>
    val weight: ObservableField<String>
    val imageUrl: ObservableField<String>
    fun init(): Completable
}