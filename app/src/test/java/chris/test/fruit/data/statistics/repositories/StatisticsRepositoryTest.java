package chris.test.fruit.data.statistics.repositories;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chris.test.fruit.datasources.network.statistics.netsources.StatisticsNetSource;
import chris.test.fruit.domain.services.statistics.models.Events;
import chris.test.fruit.rules.RxImmediateSchedulerRule;
import io.reactivex.Completable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsRepositoryTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private StatisticsNetSource mockStatisticsNetSource;
    @InjectMocks
    private StatisticsRepository sut;

    @Test
    public void whenSendStatsThenCallNetSourceSendStats() {
        final String data = "data";
        when(mockStatisticsNetSource.sendStats(Events.display.toString(), data)).thenReturn(Completable.complete());

        sut.sendStats(Events.display, data)
                .test()
                .assertNoErrors()
                .assertComplete();
        verify(mockStatisticsNetSource).sendStats(Events.display.toString(), data);
    }
}
