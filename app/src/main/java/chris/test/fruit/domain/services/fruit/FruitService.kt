package chris.test.fruit.domain.services.fruit

import chris.test.fruit.domain.services.fruit.models.Fruit
import io.reactivex.Single

class FruitService(private val fruitRepository: FruitRepositoryApi) : FruitServiceApi {

    override fun getData(): Single<List<Fruit>> {
        return fruitRepository.getData()
    }
}
