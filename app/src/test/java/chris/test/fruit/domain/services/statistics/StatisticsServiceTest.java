package chris.test.fruit.domain.services.statistics;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chris.test.fruit.rules.RxImmediateSchedulerRule;
import chris.test.fruit.data.statistics.repositories.StatisticsRepository;
import chris.test.fruit.domain.services.statistics.models.Events;
import io.reactivex.Completable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatisticsServiceTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private StatisticsRepository mockStatisticsRepository;
    @InjectMocks
    private StatisticsService sut;

    @Test
    public void whenSendStatsThenCallRepositorySendStats() {
        final String data = "data";
        when(mockStatisticsRepository.sendStats(Events.display, data)).thenReturn(Completable.complete());

        sut.sendStats(Events.display, data)
                .test()
                .assertNoErrors()
                .assertComplete();
        verify(mockStatisticsRepository).sendStats(Events.display, data);
    }
}
