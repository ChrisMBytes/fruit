package chris.test.fruit.data.fruit.repositories

import chris.test.fruit.data.fruit.repositories.mappers.FruitMapper
import chris.test.fruit.datasources.network.fruit.netsources.FruitNetSourceApi
import chris.test.fruit.domain.services.fruit.FruitRepositoryApi
import chris.test.fruit.domain.services.fruit.models.Fruit
import io.reactivex.Single

class FruitRepository(private val fruitNetSource: FruitNetSourceApi) : FruitRepositoryApi {

    override fun getData(): Single<List<Fruit>> {
        return fruitNetSource.getData()
                .map { FruitMapper.toFruits(it.fruit) }
    }
}
