package chris.test.fruit.datasources.network.statistics;

import io.reactivex.Completable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StatisticsRestServiceApi {
    @GET("stats")
    Completable sendStats(@Query("event") String event, @Query("data") String data);
}
