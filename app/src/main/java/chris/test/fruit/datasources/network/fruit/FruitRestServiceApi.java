package chris.test.fruit.datasources.network.fruit;

import chris.test.fruit.datasources.network.fruit.netsources.models.FruitListNet;
import io.reactivex.Single;
import retrofit2.http.GET;

public interface FruitRestServiceApi {
    @GET("data.json")
    Single<FruitListNet> getData();
}
