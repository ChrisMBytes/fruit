package chris.test.fruit.presentation.recyclerview.viewmodels;

public interface ItemViewModel<ITEM_T> {

    void setItem(final ITEM_T item);
}
