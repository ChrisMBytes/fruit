package chris.test.fruit.datasources.network;

public interface RetrofitConfig {
    String getBaseUrl();
}
