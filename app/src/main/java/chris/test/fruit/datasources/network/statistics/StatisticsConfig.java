package chris.test.fruit.datasources.network.statistics;

import chris.test.fruit.datasources.network.RetrofitConfig;

public class StatisticsConfig implements RetrofitConfig {
    @Override
    public String getBaseUrl() {
        return "https://raw.githubusercontent.com/fmtvp/recruit-test-data/master/";
    }
}
