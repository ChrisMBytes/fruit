package chris.test.fruit.presentation.detail.viewmodels;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chris.test.fruit.presentation.events.stores.FruitStore;
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore;
import chris.test.fruit.rules.RxImmediateSchedulerRule;
import chris.test.fruit.domain.services.fruit.models.Fruit;
import chris.test.fruit.presentation.statistics.StatisticEvents;
import chris.test.fruit.view.utils.Converter;
import io.reactivex.Completable;
import io.reactivex.Observable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DetailActivityViewModelTest {
    @Rule
    public RxImmediateSchedulerRule rxImmediateSchedulerRule = new RxImmediateSchedulerRule();
    @Mock
    private FruitStore mockFruitStore;
    @Mock
    private StatisticsMetricsStore mockStatisticsMetricsStore;
    @Mock
    private StatisticEvents mockStatisticEvents;
    @Mock
    private Fruit mockFruit;
    @InjectMocks
    private DetailActivityViewModel sut;

    @Before
    public void setup() {
        when(mockFruit.getType()).thenReturn("apple");
        when(mockFruit.getPrice()).thenReturn("10");
        when(mockFruit.getWeight()).thenReturn("100");
        when(mockFruit.getImage()).thenReturn("url");
        when(mockFruitStore.observe()).thenReturn(Observable.just(mockFruit));
        when(mockStatisticsMetricsStore.observe()).thenReturn(Observable.just(1L));
        when(mockStatisticEvents.displayEvent(anyString())).thenReturn(Completable.complete());
    }

    @Test
    public void whenInitThenCallFruitStoreObserveAndSetData() {
        sut.init()
                .test()
                .assertComplete()
                .assertNoErrors();
        verify(mockFruitStore).observe();
        assertEquals(sut.getType().get(), Converter.convertFirstLetterToUpperCase(mockFruit.getType()));
        assertEquals(sut.getPrice().get(), Converter.convertPenceToPounds(mockFruit.getPrice()));
        assertEquals(sut.getWeight().get(), Converter.convertGramsToKilograms(mockFruit.getWeight()));
        assertEquals(sut.getImageUrl().get(), mockFruit.getImage());
    }

    @Test
    public void whenInitThenCallStatisticsMetricsStoreObserve() {
        sut.init()
                .test()
                .assertComplete()
                .assertNoErrors();
        verify(mockStatisticsMetricsStore).observe();
    }

    @Test
    public void whenInitThenCallStatisticEventsDisplayEvent() {
        sut.init()
                .test()
                .assertComplete()
                .assertNoErrors();
        verify(mockStatisticEvents).displayEvent(anyString());
    }
}
