package chris.test.fruit.data.statistics.repositories

import chris.test.fruit.datasources.network.statistics.netsources.StatisticsNetSourceApi
import chris.test.fruit.domain.services.statistics.StatisticsRepositoryApi
import chris.test.fruit.domain.services.statistics.models.Events
import io.reactivex.Completable

class StatisticsRepository(private val statisticsNetSource: StatisticsNetSourceApi) : StatisticsRepositoryApi {

    override fun sendStats(event: Events, data: String): Completable {
        return statisticsNetSource.sendStats(event.toString(), data)
    }
}
