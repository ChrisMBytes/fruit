package chris.test.fruit.domain.services.statistics

import chris.test.fruit.domain.services.statistics.models.Events
import io.reactivex.Completable

class StatisticsService(private val statisticsRepository: StatisticsRepositoryApi) : StatisticsServiceApi {

    override fun sendStats(event: Events, data: String): Completable {
        return statisticsRepository.sendStats(event, data)
    }
}
