package chris.test.fruit.view.recyclerview.listeners;

import io.reactivex.Observable;

public interface RefreshAction {
    void refresh();

    Observable<Boolean> onFinished();
}
