package chris.test.fruit.presentation.home.viewmodels

import chris.test.fruit.domain.services.fruit.FruitServiceApi
import chris.test.fruit.domain.services.fruit.models.Fruit
import chris.test.fruit.presentation.events.stores.FruitStore
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore
import chris.test.fruit.presentation.statistics.StatisticEvents
import io.reactivex.Completable
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Matchers.anyString
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

class MainActivityViewModelTest {
    @Mock
    private lateinit var mockFruitServiceApi: FruitServiceApi
    @Mock
    private lateinit var mockStatisticEvents: StatisticEvents
    @Mock
    private lateinit var mockFruitStore: FruitStore
    @Mock
    private lateinit var mockStatisticsMetricsStore: StatisticsMetricsStore
    @Mock
    private lateinit var mockFruit: Fruit
    @InjectMocks
    private lateinit var sut: MainActivityViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun whenGetMockyDataThenCallMockyServiceGetDataAndStatisticEventsLoadData() {
        `when`(mockFruit.type).thenReturn("apple")
        `when`(mockFruit.price).thenReturn("10")
        `when`(mockFruit.weight).thenReturn("100")
        val fruitList = Arrays.asList(mockFruit, mockFruit)
        `when`(mockFruitServiceApi.getData()).thenReturn(Single.just(fruitList))
        `when`(mockStatisticEvents.loadEvent(anyString())).thenReturn(Completable.complete())

        sut.init()
                .test()
                .assertComplete()
                .assertNoErrors()
        verify(mockFruitServiceApi).getData()
        verify(mockStatisticEvents).loadEvent(anyString())
        assertEquals(sut.getFruitList()[0].type, mockFruit.type)
        assertEquals(sut.getFruitList()[0].price, mockFruit.price)
        assertEquals(sut.getFruitList()[0].weight, mockFruit.weight)
    }
}
