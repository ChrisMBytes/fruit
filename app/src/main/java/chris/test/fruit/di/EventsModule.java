package chris.test.fruit.di;

import javax.inject.Singleton;

import chris.test.fruit.presentation.events.stores.FruitStore;
import chris.test.fruit.presentation.events.stores.StatisticsMetricsStore;
import dagger.Module;
import dagger.Provides;

@Module
public class EventsModule {

    @Singleton
    @Provides
    FruitStore providesFruitStore() {
        return new FruitStore();
    }

    @Singleton
    @Provides
    StatisticsMetricsStore providesStatisticsMetricsStore() {
        return new StatisticsMetricsStore();
    }
}
