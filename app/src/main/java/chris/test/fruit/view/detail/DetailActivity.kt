package chris.test.fruit.view.detail

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.ActionBar

import javax.inject.Inject

import chris.test.fruit.R
import chris.test.fruit.databinding.ActivityDetailBinding
import chris.test.fruit.presentation.detail.viewmodels.DetailActivityViewModel
import chris.test.fruit.presentation.detail.viewmodels.DetailActivityViewModelApi
import chris.test.fruit.view.BaseActivity
import dagger.android.AndroidInjection

import chris.test.fruit.rx.RxTransformers.applyCompletableSchedulers
import chris.test.fruit.rx.RxTransformers.consumeCompletableError

class DetailActivity : BaseActivity() {
    @Inject
    lateinit var detailActivityViewModel: DetailActivityViewModelApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityDetailBinding>(this, R.layout.activity_detail)
        setupToolbar(binding)

        if (savedInstanceState == null) {
            AndroidInjection.inject(this)
        } else {
            detailActivityViewModel = lastCustomNonConfigurationInstance as DetailActivityViewModel
        }
        binding.viewModel = detailActivityViewModel
        disposables.add(detailActivityViewModel.init()
                .compose(applyCompletableSchedulers())
                .compose(consumeCompletableError())
                .subscribe())
    }

    private fun setupToolbar(binding: ActivityDetailBinding) {
        setSupportActionBar(binding.fruitToolbar)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onRetainCustomNonConfigurationInstance(): Any {
        return detailActivityViewModel
    }
}
