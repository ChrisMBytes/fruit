package chris.test.fruit.datasources.network.statistics.netsources

import io.reactivex.Completable

interface StatisticsNetSourceApi {
    fun sendStats(event: String, data: String): Completable
}