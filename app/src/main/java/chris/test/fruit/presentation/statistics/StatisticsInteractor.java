package chris.test.fruit.presentation.statistics;

import chris.test.fruit.domain.services.statistics.StatisticsServiceApi;
import chris.test.fruit.domain.services.statistics.models.Events;
import io.reactivex.Completable;

public class StatisticsInteractor implements StatisticEvents {
    private final StatisticsServiceApi statisticsService;

    public StatisticsInteractor(StatisticsServiceApi statisticsService) {
        this.statisticsService = statisticsService;
    }

    @Override
    public Completable loadEvent(String data) {
        return statisticsService.sendStats(Events.load, data);
    }

    @Override
    public Completable displayEvent(String data) {
        return statisticsService.sendStats(Events.display, data);
    }

    @Override
    public Completable errorEvent(String data) {
        return statisticsService.sendStats(Events.error, data);
    }
}
