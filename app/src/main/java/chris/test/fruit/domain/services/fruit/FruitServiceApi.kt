package chris.test.fruit.domain.services.fruit

import chris.test.fruit.domain.services.fruit.models.Fruit
import io.reactivex.Single

interface FruitServiceApi {
    fun getData(): Single<List<Fruit>>
}
